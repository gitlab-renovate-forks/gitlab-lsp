import { WebviewId } from '@gitlab-org/webview-plugin';

export const WEBVIEW_ID = 'chat' as WebviewId;
export const WEBVIEW_TITLE = 'GitLab: Duo Chat';
