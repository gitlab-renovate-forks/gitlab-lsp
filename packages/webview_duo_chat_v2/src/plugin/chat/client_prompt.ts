import { ChatRecordType } from './gitlab_chat_record';

export type PromptType = Exclude<ChatRecordType, 'general'>;
type PromptTypeWithSlashCommand = Exclude<PromptType, 'focusChat'>;

export const commandToContentMap: Record<PromptTypeWithSlashCommand, string> = {
  explainCode: '/explain',
  fixCode: '/fix',
  generateTests: '/tests',
  refactorCode: '/refactor',
  newConversation: '/reset',
} as const;

export const validPromptTypes: PromptType[] = [
  'explainCode',
  'generateTests',
  'refactorCode',
  'newConversation',
  'fixCode',
  'focusChat',
] as const;
