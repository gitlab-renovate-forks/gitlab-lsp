import { ApiRequest } from '../platform/web_ide';

export type GitLabVersionResponse = {
  version: string;
  enterprise?: boolean;
};
export const versionRequest: ApiRequest<GitLabVersionResponse> = {
  type: 'rest',
  method: 'GET',
  path: '/api/v4/version',
};
