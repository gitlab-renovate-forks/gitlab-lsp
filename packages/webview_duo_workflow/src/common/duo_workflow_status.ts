export enum DuoWorkflowStatus {
  CREATED = 'CREATED',
  RUNNING = 'RUNNING',
  FINISHED = 'FINISHED',
  FAILED = 'FAILED',
  STOPPED = 'STOPPED',
  INPUT_REQUIRED = 'INPUT_REQUIRED',
  PLAN_APPROVAL = 'PLAN_APPROVAL_REQUIRED',
}

export const isTerminated = (status: DuoWorkflowStatus) =>
  [DuoWorkflowStatus.FINISHED, DuoWorkflowStatus.FAILED, DuoWorkflowStatus.STOPPED].includes(
    status,
  );

export const isRunning = (status: DuoWorkflowStatus) => !isTerminated(status);

export const isApprovingPlan = (status: DuoWorkflowStatus) =>
  status === DuoWorkflowStatus.PLAN_APPROVAL;
export const isNeedsInput = (status: DuoWorkflowStatus) =>
  status === DuoWorkflowStatus.INPUT_REQUIRED;
export const isAwaitingUserInput = (status: DuoWorkflowStatus) =>
  isApprovingPlan(status) || isNeedsInput(status);
