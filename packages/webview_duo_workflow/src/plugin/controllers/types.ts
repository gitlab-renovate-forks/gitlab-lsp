import { WorkflowGraphqlPayload } from '@gitlab-lsp/workflow-api';
import { DuoWorkflowMessage, DuoWorkflowStatus, ParsedDuoWorkflowEvent } from '../../types';
import { WorkflowEvent } from '../../common/duo_workflow_events';
import { NO_REPLY } from './constants';

export type getWorkflowParams = { workflowId: string };

export type sendWorkflowEventParams = {
  eventType: WorkflowEvent;
  workflowId: string;
  message: DuoWorkflowMessage;
};
export type openUrlParams = { url: string };
export type openFileParams = { filePath: string };

export type ControllerResponse =
  | { eventName: 'workflowStatus'; data: DuoWorkflowStatus }
  | { eventName: 'dockerConfigured'; data: boolean }
  | { eventName: 'isDockerImageAvailable'; data: boolean }
  | { eventName: 'workflowCheckpoint'; data: ParsedDuoWorkflowEvent }
  | { eventName: 'pullDockerImageCompleted'; data: { success: boolean } }
  | { eventName: 'workflowError'; data: string }
  | { eventName: 'workflowStarted'; data: string }
  | { eventName: 'workflowGoal'; data: string }
  | { eventName: 'setProjectPath'; data: string };

export interface WorkflowGraphqlPayloadClient extends WorkflowGraphqlPayload {
  eventName: ControllerResponse['eventName'];
}

export type ControllerNoReply = typeof NO_REPLY;

export type ControllerData =
  | ControllerResponse
  | (Promise<ControllerResponse | ControllerNoReply | ControllerResponse[]> | ControllerResponse)[]
  | ControllerNoReply;

export type ControllerResponseEnum = Promise<ControllerData> | ControllerNoReply;
