import { Connection } from 'vscode-languageserver';
import { ControllerNoReply, openUrlParams, openFileParams } from './types';
import { NO_REPLY } from './constants';

export const initWorkflowConnectionController = (connection: Connection) => {
  return {
    async openUrl({ url }: openUrlParams): Promise<ControllerNoReply> {
      await connection.sendNotification('$/gitlab/openUrl', { url });
      return NO_REPLY;
    },
    async openFile({ filePath }: openFileParams): Promise<ControllerNoReply> {
      await connection.sendNotification('$/gitlab/openFile', { filePath });
      return NO_REPLY;
    },
  };
};
