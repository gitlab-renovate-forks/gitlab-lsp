import { shallowMount } from '@vue/test-utils';
import { createTestingPinia } from '@pinia/testing';
import { useMainStore } from '../../../../stores/main';
import BaseMessage from './base.vue';
import SystemMessage from './system.vue';

describe('SystemMessage', () => {
  let wrapper;
  let mainStore;

  const mockEvent = {
    preventDefault: jest.fn(),
  };

  const createComponent = ({ props = {}, mountFn = shallowMount } = {}) => {
    wrapper = mountFn(SystemMessage, {
      propsData: {
        message: {
          content: 'Test message',
          role: 'system',
          tool_info: { args: { file_path: '/path/to/file.js' } },
          ...props,
        },
        ...props,
      },
      global: {
        provide: {
          renderGFM: () => jest.fn(),
        },
        global: {
          plugins: [createTestingPinia()],
        },
      },
    });

    mainStore = useMainStore();
  };

  const findBaseMessage = () => wrapper.findComponent(BaseMessage);
  const findFilePathLink = () => wrapper.find('[data-testid="file-path-link"]');

  beforeEach(() => {
    createComponent();
  });

  it('renders the base message component', () => {
    expect(findBaseMessage().exists()).toBe(true);
  });

  describe('file path handling', () => {
    it('renders file path link when file path is present', () => {
      expect(findFilePathLink().exists()).toBe(true);
      expect(findFilePathLink().text()).toBe('/path/to/file.js');
    });

    it('does not render file path link when file path is absent', async () => {
      await createComponent({
        props: {
          message: {
            tool_info: { args: { something_else: 'test' } },
          },
        },
      });

      expect(findFilePathLink().exists()).toBe(false);
    });

    it('calls openFile method when clicked', async () => {
      await findFilePathLink().vm.$emit('click', mockEvent);
      expect(mockEvent.preventDefault).toHaveBeenCalled();
      expect(mainStore.openFile).toHaveBeenCalledWith('/path/to/file.js');
    });
  });
});
