import { shallowMount } from '@vue/test-utils';
import { createTestingPinia } from '@pinia/testing';
import { DuoWorkflowStatus } from '../../../../types/index.ts';
import DuoWorkflowCheckpoint from './duo_workflow_checkpoint.vue';
import CheckpointSkeletonLoader from './checkpoint_skeleton_loader.vue';
import StepStatusBadge from './step_status_badge.vue';

describe('DuoWorkflowCheckpoint', () => {
  let wrapper;
  let pinia;

  const defaultSteps = [
    { description: 'Find stuff', status: 'Completed' },
    { description: 'Change code', status: 'running' },
  ];

  const createWrapper = ({ props = {}, steps = defaultSteps } = {}) => {
    pinia = createTestingPinia();

    wrapper = shallowMount(DuoWorkflowCheckpoint, {
      propsData: {
        status: props.status || DuoWorkflowStatus.RUNNING,
        checkpoint: {
          ts: '2024-01-01T12:00:00Z',
          channel_values: {
            plan: {
              steps,
            },
            status: 'Planning',
          },
        },
      },
      global: {
        plugins: [pinia],
      },
    });
  };

  const findSkeletonLoader = () => wrapper.findComponent(CheckpointSkeletonLoader);
  const findSteps = () => wrapper.findAll('li');
  const findStatusBadges = () => wrapper.findAllComponents(StepStatusBadge);

  describe('when loading', () => {
    beforeEach(() => {
      createWrapper({ steps: [] });
    });

    it('should render the skeleton loader', () => {
      expect(findSkeletonLoader().exists()).toBe(true);
    });

    it('does not render any steps', () => {
      expect(findSteps()).toHaveLength(0);
    });
  });

  describe('when there are steps', () => {
    beforeEach(() => {
      createWrapper();
    });

    it('does not render the skeleton loader', () => {
      expect(findSkeletonLoader().exists()).toBe(false);
    });

    it('renders the steps', () => {
      expect(findSteps()).toHaveLength(2);
    });

    it('renders the status badge', () => {
      expect(findStatusBadges()).toHaveLength(2);
    });
  });

  describe('currentStep', () => {
    describe('when status is terminated', () => {
      beforeEach(() => {
        createWrapper({
          props: { status: DuoWorkflowStatus.FAILED },
        });
      });

      it('does not highlight any step', () => {
        findSteps().wrappers.forEach((step) => {
          expect(step.classes()).not.toContain('selected-item');
        });
      });
    });

    describe('when first step is not started', () => {
      beforeEach(() => {
        createWrapper({
          steps: [{ description: 'Not started step', status: 'Not Started' }, ...defaultSteps],
        });
      });

      it('does not highlight any step', () => {
        findSteps().wrappers.forEach((step) => {
          expect(step.classes()).not.toContain('selected-item');
        });
      });
    });

    describe('when the first step is started', () => {
      beforeEach(() => {
        createWrapper({
          steps: [{ description: 'ok', status: 'Running' }, ...defaultSteps],
        });
      });

      it('highlights the step in progress', () => {
        expect(findSteps().at(0).classes()).toContain('selected-item');
        expect(findSteps().at(1).classes()).not.toContain('selected-item');
      });
    });
  });
});
