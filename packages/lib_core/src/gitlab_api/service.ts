import { Cable as ActionCableCable } from '@anycable/core';
import { createInterfaceId } from '@gitlab/needle';
import { Disposable } from '@gitlab-org/disposable';
import { ApiReconfiguredData, ApiRequest, InstanceInfo, TokenInfo } from './types';

export interface GitLabApiService {
  fetchFromApi<TReturnType>(request: ApiRequest<TReturnType>): Promise<TReturnType>;
  connectToCable(): Promise<ActionCableCable>;
  onApiReconfigured(listener: (data: ApiReconfiguredData) => void): Disposable;
  readonly instanceInfo?: InstanceInfo;
  readonly tokenInfo?: TokenInfo;
}

export const GitLabApiService = createInterfaceId<GitLabApiService>('GitLabApiService');
