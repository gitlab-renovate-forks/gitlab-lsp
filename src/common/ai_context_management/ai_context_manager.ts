import { createCollectionId, Injectable } from '@gitlab/needle';
import {
  AIContextItem,
  AIContextCategory,
  AIContextProviderType,
  AIContextManager,
  AIContextSearchRequest,
  AIContextRetrieveRequest,
} from '@gitlab-org/ai-context';
import { WorkspaceFolder } from 'vscode-languageserver-types';
import { Logger, withPrefix } from '@gitlab-org/logging';
import { ConfigService } from '../config_service';
import { log } from '../log';
import { AiContextTransformerService } from './context_transformers/ai_context_transformer_service';
import { DuoChatAIRequest } from './ai_context_provider';
import { AIContextProvider } from '.';

const AIContextMapping: Record<AIContextCategory, AIContextProviderType[]> = {
  file: ['open_tab', 'local_file_search'],
  snippet: ['snippet'],
  issue: ['issue'],
  merge_request: ['merge_request'],
  dependency: ['dependency'],
  local_git: ['local_git'],
};

export const AiContextProviderCollection = createCollectionId(AIContextProvider);

@Injectable(AIContextManager, [
  Logger,
  ConfigService,
  AiContextTransformerService,
  AiContextProviderCollection,
])
export class DefaultAIContextManager implements AIContextManager {
  readonly #providers: AIContextProvider<AIContextItem>[] = [];

  #availableProviders: AIContextProvider<AIContextItem>[] = [];

  readonly #transformerService: AiContextTransformerService;

  #workspaceFolders: WorkspaceFolder[];

  readonly #logger: Logger;

  constructor(
    logger: Logger,
    configService: ConfigService,
    aiContextTransformerService: AiContextTransformerService,
    providers: AIContextProvider[],
  ) {
    this.#logger = withPrefix(logger, 'AIContextManager');
    this.#transformerService = aiContextTransformerService;
    this.#providers = providers;

    this.#workspaceFolders = configService.get('client.workspaceFolders') ?? [];
    configService.onConfigChange((config) => {
      this.#workspaceFolders = config.client.workspaceFolders ?? [];
    });
  }

  async #getProviderAndPerform(
    subType: AIContextProviderType | undefined,
    callback: (provider: AIContextProvider<AIContextItem>) => Promise<void>,
  ): Promise<void> {
    const e = new Error('No provider found for type');
    const provider = this.#availableProviders.find((p) => p.type === subType);
    if (!provider) {
      throw e;
    }
    await callback(provider);
  }

  #getProviderForItem(item: AIContextItem): AIContextProvider<AIContextItem> {
    const foundProvider = this.#availableProviders.find(
      (provider) => provider.type === item.metadata?.subType,
    );
    if (!foundProvider) {
      throw new Error(
        `No provider found for type "${item.metadata.subType || 'undefined item subtype'}"`,
      );
    }
    return foundProvider;
  }

  async addSelectedContextItem(contextItem: AIContextItem): Promise<boolean> {
    // FIXME: aiContextManager should return the result of the operation upstream,
    // the methods should also be async https://gitlab.com/gitlab-org/gitlab/-/issues/489292
    try {
      this.#logger.info(`received context item add request ${contextItem.id}`);
      await this.#getProviderAndPerform(contextItem.metadata?.subType, async (provider) => {
        await provider.addSelectedContextItem(contextItem);
      });
      this.#logger.info(`added item result ${true}`);
      return true;
    } catch (e) {
      this.#logger.error(`error adding context item`, e);
      return false;
    }
  }

  async removeSelectedContextItem(contextItem: AIContextItem): Promise<boolean> {
    log.info(`received context item remove request ${contextItem.id}`);
    try {
      await this.#getProviderAndPerform(contextItem.metadata?.subType, async (provider) => {
        await provider.removeSelectedContextItem(contextItem.id);
      });
      log.info(`removed item result ${true}`);
      return true;
    } catch (e) {
      log.error(`error removing context item`, e);
      return false;
    }
  }

  async getSelectedContextItems(): Promise<AIContextItem[]> {
    const currentItems = (
      await Promise.all(
        this.#availableProviders.map((provider) => provider.getSelectedContextItems()),
      )
    ).flat();
    log.info(`returning ${currentItems.length} current context items`);
    return currentItems;
  }

  /**
   * TODO: check Open Tabs availability through isAvailable() like the other providers.
   * The `duoRequiredFeature` of DefaultOpenTabContextProvider is tied to a Duo Chat feature,
   * so we can't check availability through the provider's isAvailable() function.
   * For now, we will just include Open Tabs contexts by default for Code Suggestions.
   * Later on, we will need to refactor the DefaultOpenTabContextProvider to set
   * the required feature according to the feature type (Duo Chat or Code Suggestions)
   */
  async #getCodeSuggestionsProviders(
    aiRequest: AIContextSearchRequest | AIContextRetrieveRequest<AIContextItem>,
  ): Promise<AIContextProvider<AIContextItem>[]> {
    const providers = await Promise.all(
      this.#providers.map(async (provider) => {
        const isOpenTabProvider = provider.type === 'open_tab';
        // Always include OpenTabContextProvider for code suggestions
        const isAvailableResult = isOpenTabProvider
          ? true
          : (await provider.isAvailable(aiRequest)).enabled;
        return {
          provider,
          isAvailable: isAvailableResult,
        };
      }),
    ).then((results) =>
      results.filter((result) => result.isAvailable).map((result) => result.provider),
    );

    return providers;
  }

  async searchContextItems(aiRequest: AIContextSearchRequest): Promise<AIContextItem[]> {
    switch (aiRequest.featureType) {
      case 'code_suggestions': {
        const providers = await this.#getCodeSuggestionsProviders(aiRequest);
        const contextItems = await Promise.all(
          providers.map((provider) => provider.searchContextItems(aiRequest)),
        );
        return contextItems.flat();
      }
      case 'duo_chat': {
        const request = aiRequest as DuoChatAIRequest;

        log.info(
          `received context search query, category: ${request.category}, query: ${request.query}`,
        );
        try {
          const contextItems: AIContextItem[] = [];
          const typesForCategory = AIContextMapping[request.category];
          const workspaceFolders = request.workspaceFolders ?? this.#workspaceFolders;
          if (!workspaceFolders.length) {
            log.debug(`No workspace folders detected.`);
          }

          const providerQuery = {
            ...request,
            workspaceFolders,
          };

          const contextItemsPromises = this.#availableProviders
            .filter((p) => typesForCategory.includes(p.type))
            .map(async (provider) => {
              return provider.searchContextItems(providerQuery);
            });

          const contextItemsForAllProviders = await Promise.all(contextItemsPromises);
          contextItemsForAllProviders.forEach((items) => contextItems.push(...items));
          // TODO: add params to enable/disable the logic below
          // https://gitlab.com/gitlab-org/gitlab/-/issues/489466
          const selectedContextItemsSet = new Set(
            (await this.getSelectedContextItems())
              .map((item) => item?.id)
              .filter((id) => id !== undefined),
          );
          // Filter out the selected context items from the context items
          const nonSelectedItems = contextItems.filter(
            (item) => !selectedContextItemsSet.has(item.id),
          );

          const results = this.#disabledContextItemsToTheBack(nonSelectedItems);
          log.info(`context search query had ${results.length} results`);
          return results;
        } catch (e) {
          log.error(`error searching context items`, e);
          return [];
        }
      }
      default: {
        throw new Error(`Unsupported AI request feature type: ${aiRequest.featureType}`);
      }
    }
  }

  #disabledContextItemsToTheBack(contextItems: AIContextItem[]) {
    return contextItems.sort((a, b) => {
      if (a.metadata?.enabled === b.metadata?.enabled) return 0;
      return a.metadata?.enabled ? -1 : 1;
    });
  }

  async getAvailableCategories(): Promise<AIContextCategory[]> {
    const providerPromises = this.#providers.map(async (provider) => {
      const { enabled } = await provider.isAvailable();
      return enabled ? provider : null;
    });
    const providers = await Promise.all(providerPromises);
    this.#availableProviders = providers.filter(Boolean) as AIContextProvider[];

    const types = this.#availableProviders.map((provider) => provider.type);
    const categories = Object.keys(AIContextMapping) as AIContextCategory[];
    const availableCategories = types
      .map((type) =>
        categories.find((category) =>
          AIContextMapping[category].includes(type as AIContextProviderType),
        ),
      )
      .filter((category) => category !== undefined);

    this.#logger.info(
      `returning ${availableCategories.length} available context provider categories: "${availableCategories.join(', ')}"`,
    );

    return availableCategories;
  }

  /**
   * Retrieves context items with content for the given AI request.
   *
   * @deprecated - TODO: We are going to redesign the overall architecture to be modular at the feature level,
   * see https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp/-/issues/752#note_2310700284
   *
   */
  async retrieveContextItemsWithContent(
    request?: AIContextRetrieveRequest<AIContextItem>,
  ): Promise<AIContextItem[]> {
    if (request?.featureType === 'code_suggestions') {
      const codeSuggestionProviders = await this.#getCodeSuggestionsProviders(request);
      const groupedItems = request.aiContextItems.reduce((acc, item) => {
        const itemType = item.metadata?.subType;
        const provider = codeSuggestionProviders.find((p) => p.type === itemType);

        if (provider) {
          const existingItems = acc.get(provider.type) ?? [];
          acc.set(provider.type, [...existingItems, item]);
        } else {
          log.error(`No provider found for type "${itemType}"`);
        }

        return acc;
      }, new Map<string, typeof request.aiContextItems>());

      const providersWithContext = await Promise.all(
        codeSuggestionProviders.map((provider) =>
          provider.retrieveContextItemsWithContent({
            featureType: 'code_suggestions',
            aiContextItems: groupedItems.get(provider.type) ?? [],
          }),
        ),
      );
      const contextItems = providersWithContext.flat().filter(Boolean);
      return Promise.all(
        contextItems.map((contextItem) => this.#transformItemContent(contextItem)),
      );
    }
    const providersWithContext = await Promise.all(
      this.#availableProviders.map((provider) => provider.retrieveContextItemsWithContent()),
    );
    const contextItems = providersWithContext.flat().filter(Boolean);
    return Promise.all(contextItems.map((contextItem) => this.#transformItemContent(contextItem)));
  }

  clearSelectedContextItems(): boolean {
    this.#logger.info('clearing all selected context items');
    this.#availableProviders.forEach((provider) => provider.clearSelectedContextItems());
    return true;
  }

  async getItemWithContent(item: AIContextItem): Promise<AIContextItem> {
    this.#logger.info(`received get context item content request ${item.id}`);
    const provider = this.#getProviderForItem(item);
    const itemWithContent = await provider.getItemWithContent(item);
    this.#logger.info(`retrieved context item content ${itemWithContent}`);
    return this.#transformItemContent(itemWithContent);
  }

  async #transformItemContent(item: AIContextItem): Promise<AIContextItem> {
    try {
      return await this.#transformerService.transform(item);
    } catch (error) {
      // Since content transformation failed, we strip out the content completely.
      // This is to ensure we don't send sensitive values (e.g. because our secret redaction transformer failed)
      this.#logger.error(
        `Error transforming context item content. Item content will be excluded.`,
        error,
      );
      return {
        ...item,
        content: undefined,
      };
    }
  }

  async getProviderForType(type: AIContextProviderType): Promise<AIContextProvider> {
    const provider = this.#providers.find((p) => p.type === type);
    if (!provider) {
      throw new Error(`No provider found for type "${type}"`);
    }
    return provider;
  }
}
