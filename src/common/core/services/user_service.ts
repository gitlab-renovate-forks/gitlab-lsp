import { gql } from 'graphql-request';
import { Disposable } from '@gitlab-org/disposable';
import { Injectable } from '@gitlab/needle';
import { UserService, GitLabUser, GraphQLRequest } from '@gitlab-org/core';
import { GitLabApiClient } from '../../api';
import { GitLabGID, tryParseGitLabGid } from '../../graphql/gid_utils';
import { log } from '../../log';

interface GqlCurrentUser {
  currentUser: {
    id: GitLabGID;
    username: string;
    name: string;
  };
}

const getCurrentUserQuery: GraphQLRequest<GqlCurrentUser> = {
  type: 'graphql',
  query: gql`
    query getUser {
      currentUser {
        id
        username
        name
      }
    }
  `,
  variables: {},
};
@Injectable(UserService, [GitLabApiClient])
export class DefaultUserService implements UserService, Disposable {
  #subscriptions: Disposable[] = [];

  user?: GitLabUser;

  constructor(apiClient: GitLabApiClient) {
    this.#subscriptions.push(
      apiClient.onApiReconfigured(async (data) => {
        if (!data.isInValidState) {
          this.user = undefined;
          return;
        }

        try {
          const gqlUser = await apiClient.fetchFromApi(getCurrentUserQuery);
          const { id, username, name } = gqlUser.currentUser;

          const restId = tryParseGitLabGid(id);
          if (!restId) throw new Error(`Failed to parse user GID into REST ID: "${id}"`);

          this.user = {
            gqlId: id,
            restId,
            username,
            name,
          };
          log.debug(`[UserService] new user fetched: ${JSON.stringify(this.user)}`);
        } catch (e) {
          log.error('[UserService] Failed to get user from API', e);
          this.user = undefined;
        }
      }),
    );
  }

  dispose = () => this.#subscriptions.forEach((d) => d.dispose());
}
