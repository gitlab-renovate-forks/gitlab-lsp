import EventEmitter from 'events';
import { compare as semverCompare } from 'semver';
import { CancellationToken, Disposable } from 'vscode-languageserver-protocol';
import { Cable as ActionCableCable } from '@anycable/core';
import { Injectable, createInterfaceId } from '@gitlab/needle';
import {
  InstanceInfo,
  TokenInfo,
  ApiReconfiguredData,
  GetRequest,
  PostRequest,
  ApiRequest,
} from '@gitlab-org/core';
import { ConfigService } from './config_service';
import {
  ICodeSuggestionModel,
  ClientInfo,
} from './tracking/code_suggestions/code_suggestions_tracking_types';
import { LsFetch } from './fetch';
import { log } from './log';
import { handleFetchError } from './handle_fetch_error';
import { GITLAB_API_BASE_URL } from './constants';
import { AdditionalContext, SuggestionOption, versionRequest } from './api_types';
import { FetchError, InvalidInstanceVersionError } from './fetch_error';
import { connectToCable } from './action_cable';
import { SimpleApiClient, DefaultSimpleApiClient } from './simple_api_client';

const REQUIRED_SCOPE = 'api';

/**
 * @deprecated This interface and implementation are being phased out in favor of the new GitLabApiService from the `@gitlab-org/core` package instead.
 *
 * Reasons for deprecation:
 * 1. Better separation of concerns as we move away from monolithic clients and services
 * 2. Support for declare dependencies on the core api service from packages and plugins
 * 3. Improved maintainability and testability as we move towards a more modular codebase
 *
 * Migration guide:
 * 1. Import GitLabApiService from '@gitlab-org/core'
 * 2. Use dependency injection to get an instance
 * 3. Use the new service methods for API interactions
 *
 * Example usage:
 * ```typescript
 * import { GitLabApiService } from '@gitlab-org/core';
 *
 * @Injectable(MyService, [GitLabApiService])
 * class MyService {
 *   #apiService: GitLabApiService;
 *
 *   constructor(apiService: GitLabApiService) {
 *     this.#apiService = apiService;
 *   }
 * }
 * ```
 */
export interface GitLabApiClient {
  checkToken(baseURL: string | undefined, token: string | undefined): Promise<TokenCheckResponse>;
  getCodeSuggestions(request: CodeSuggestionRequest): Promise<CodeSuggestionResponse | undefined>;
  getStreamingCodeSuggestions(
    request: CodeSuggestionRequest,
    cancellationToken: CancellationToken,
  ):
    | AsyncGenerator<
        {
          chunk: string;
          serverSentEvents: boolean;
        },
        void,
        void
      >
    | undefined;
  fetchFromApi<TReturnType>(request: ApiRequest<TReturnType>): Promise<TReturnType>;
  connectToCable(): Promise<ActionCableCable>;
  onApiReconfigured(listener: (data: ApiReconfiguredData) => void): Disposable;
  getSimpleClient(baseUrl: string, token: string): SimpleApiClient;
  readonly instanceInfo?: InstanceInfo;
  readonly tokenInfo?: TokenInfo;
}

export const GitLabApiClient = createInterfaceId<GitLabApiClient>('GitLabApiClient');

export type GenerationType = 'comment' | 'small_file' | 'empty_function' | undefined;

export interface CodeSuggestionRequest {
  prompt_version: number;
  project_path: string;
  model_provider?: string;
  project_id: number;
  current_file: CodeSuggestionRequestCurrentFile;
  intent?: 'completion' | 'generation';
  stream?: boolean;
  /** how many suggestion options should the API return */
  choices_count?: number;
  /** additional context to provide to the model */
  context?: AdditionalContext[];
  /* A user's instructions for code suggestions. */
  user_instruction?: string;
  /* The backend can add additional prompt info based on the type of event */
  generation_type?: GenerationType;
}

export interface CodeSuggestionRequestCurrentFile {
  file_name: string;
  content_above_cursor: string;
  content_below_cursor: string;
}

export interface CodeSuggestionResponse {
  choices?: SuggestionOption[];
  model?: ICodeSuggestionModel;
  status: number;
  error?: string;
  isDirectConnection?: boolean;
}

// FIXME: Rename to SuggestionOptionStream when renaming the SuggestionOption
export interface StartStreamOption {
  uniqueTrackingId: string;
  /** the streamId represents the beginning of a stream * */
  streamId: string;
}

export type SuggestionOptionOrStream = SuggestionOption | StartStreamOption;

export interface PersonalAccessToken {
  scopes: string[];
}

export interface OAuthTokenInfoResponse {
  scope: string[];
}

export type ValidTokenCheckResponse = {
  valid: true;
  tokenInfo: TokenInfo;
};

export type InvalidTokenCheckResponse = {
  valid: false;
  reason: 'unknown' | 'invalid_token' | 'invalid_scopes';
  message: string;
};

export type TokenCheckResponse = ValidTokenCheckResponse | InvalidTokenCheckResponse;

export interface IDirectConnectionDetailsHeaders {
  'X-Gitlab-Global-User-Id': string;
  'X-Gitlab-Instance-Id': string;
  'X-Gitlab-Host-Name': string;
  'X-Gitlab-Saas-Duo-Pro-Namespace-Ids': string;
}

export interface IDirectConnectionModelDetails {
  model_provider: string;
  model_name: string;
}

export interface IDirectConnectionDetails {
  base_url: string;
  token: string;
  expires_at: number;
  headers: IDirectConnectionDetailsHeaders;
  model_details: IDirectConnectionModelDetails;
}

const CONFIG_CHANGE_EVENT_NAME = 'apiReconfigured';

const handleTokenCheckError = (e: unknown): InvalidTokenCheckResponse => {
  if (!(e instanceof FetchError) || !e.isInvalidToken) {
    log.warn('Error when checking a token validity', e);
    return { valid: false, reason: 'unknown', message: `Token validation failed: ${e}` };
  }
  return { valid: false, reason: 'invalid_token', message: 'Token is invalid or expired.' };
};

const validateScopes = (scopes: string[]): InvalidTokenCheckResponse | undefined => {
  if (scopes.includes(REQUIRED_SCOPE)) return undefined;

  const joinedScopes = scopes.map((scope) => `'${scope}'`).join(', ');

  return {
    valid: false,
    reason: 'invalid_scopes',
    message: `Token has scope(s) ${joinedScopes} (needs ${REQUIRED_SCOPE}).`,
  };
};

@Injectable(GitLabApiClient, [LsFetch, ConfigService])
export class GitLabAPI implements GitLabApiClient {
  #token: string | undefined;

  #baseURL: string;

  #clientInfo?: ClientInfo;

  #lsFetch: LsFetch;

  #eventEmitter = new EventEmitter();

  #configService: ConfigService;

  #instanceInfo?: InstanceInfo;

  #tokenInfo?: TokenInfo;

  getSimpleClient(baseUrl: string, token: string): SimpleApiClient {
    return new DefaultSimpleApiClient(this.#lsFetch, this.#clientInfo, baseUrl, token);
  }

  constructor(lsFetch: LsFetch, configService: ConfigService) {
    this.#baseURL = GITLAB_API_BASE_URL;
    this.#lsFetch = lsFetch;
    this.#configService = configService;
    this.#configService.onConfigChange(async (config) => {
      this.#clientInfo = configService.get('client.clientInfo');
      this.#lsFetch.updateAgentOptions({
        ignoreCertificateErrors: this.#configService.get('client.ignoreCertificateErrors') ?? false,
        ...(this.#configService.get('client.httpAgentOptions') ?? {}),
      });
      await this.configureApi(config.client);
    });
  }

  onApiReconfigured(listener: (data: ApiReconfiguredData) => void): Disposable {
    this.#eventEmitter.on(CONFIG_CHANGE_EVENT_NAME, listener);
    return { dispose: () => this.#eventEmitter.removeListener(CONFIG_CHANGE_EVENT_NAME, listener) };
  }

  #fireApiReconfigured(data: ApiReconfiguredData) {
    this.#eventEmitter.emit(CONFIG_CHANGE_EVENT_NAME, data);
  }

  async configureApi({
    token,
    baseUrl = GITLAB_API_BASE_URL,
  }: {
    token?: string;
    baseUrl?: string;
  }) {
    if (this.#token === token && this.#baseURL === baseUrl) return;

    this.#token = token;
    this.#baseURL = baseUrl;

    const tokenCheckResult = await this.checkToken(this.#baseURL, this.#token);
    let validationMessage;
    if (!tokenCheckResult.valid) {
      this.#configService.set('client.token', undefined);

      validationMessage = `Token is invalid. ${tokenCheckResult.message}. Reason: ${tokenCheckResult.reason}`;
      log.warn(validationMessage);
      this.#fireApiReconfigured({ isInValidState: false, validationMessage });
      return;
    }

    // FIXME: If this call fails, we won't fire the API reconfigured event.
    // WE should retry this call if it fails and when it fails X times, we should fire the API reconfigured event with an error message.
    const instanceVersion = await this.#getGitLabInstanceVersion(this.#baseURL, this.#token);
    const instanceInfo = {
      instanceVersion,
      instanceUrl: this.#baseURL,
    };
    this.#instanceInfo = instanceInfo;

    this.#tokenInfo = tokenCheckResult.tokenInfo;

    this.#fireApiReconfigured({
      isInValidState: true,
      instanceInfo,
      tokenInfo: tokenCheckResult.tokenInfo,
    });
  }

  get instanceInfo() {
    return this.#instanceInfo;
  }

  get tokenInfo() {
    return this.#tokenInfo;
  }

  async #checkPatToken(baseUrl: string, token: string): Promise<TokenCheckResponse> {
    const request: GetRequest<PersonalAccessToken> = {
      type: 'rest',
      method: 'GET',
      path: '/api/v4/personal_access_tokens/self',
    };
    const { scopes } = await this.getSimpleClient(baseUrl, token).fetchFromApi(request);

    const scopeValidationError = validateScopes(scopes);
    if (scopeValidationError) return scopeValidationError;

    return { valid: true, tokenInfo: { scopes, type: 'pat' } };
  }

  async #checkOAuthToken(baseUrl: string, token: string): Promise<TokenCheckResponse> {
    const request: GetRequest<OAuthTokenInfoResponse> = {
      type: 'rest',
      method: 'GET',
      path: '/oauth/token/info',
    };

    const { scope: scopes } = await this.getSimpleClient(baseUrl, token).fetchFromApi(request);
    const scopeValidationError = validateScopes(scopes);
    if (scopeValidationError) return scopeValidationError;

    return { valid: true, tokenInfo: { scopes, type: 'oauth' } };
  }

  async checkToken(
    baseURL: string = this.#baseURL,
    token: string = '',
  ): Promise<TokenCheckResponse> {
    const responses = await Promise.all([
      this.#checkPatToken(baseURL, token).catch(handleTokenCheckError),
      this.#checkOAuthToken(baseURL, token).catch(handleTokenCheckError),
    ]);

    // if the token is valid PAT or OAuth
    const validResponse = responses.find((r) => r.valid);
    if (validResponse) return validResponse;

    // api scope error is more precise than standard error
    const invalidScopeError = responses.find((r) => !r.valid && r.reason === 'invalid_scopes');
    if (invalidScopeError) return invalidScopeError;
    // invalid token is the last error type before we mark it as unknown
    const invalidTokenError = responses.find((r) => !r.valid && r.reason === 'invalid_token');
    if (invalidTokenError) return invalidTokenError;
    return responses[0];
  }

  async getCodeSuggestions(
    codeSuggestionRequest: CodeSuggestionRequest,
  ): Promise<CodeSuggestionResponse | undefined> {
    if (!this.#token) {
      throw new Error('Token needs to be provided to request Code Suggestions');
    }

    const request: PostRequest<CodeSuggestionResponse> = {
      type: 'rest',
      method: 'POST',
      path: '/api/v4/code_suggestions/completions',
      body: codeSuggestionRequest,
    };

    const response = await this.getSimpleClient(this.#baseURL, this.#token).fetchFromApi(request);

    return { ...response, status: 200 };
  }

  /**
   * Calls the code suggestions API with the given request,
   * and yields chunks of the stream as strings,
   * as the caller is responsible for aggregating them.
   *
   * We let the server know that this client supports SSE streaming by setting the `X-Supports-Sse-Streaming` header.
   *
   * We use the `X-Streaming-Format` header to determine if the server supports SSE streaming.
   * The caller is responsible for parsing the SSE events.
   */
  async *getStreamingCodeSuggestions(
    request: CodeSuggestionRequest,
    cancellationToken: CancellationToken,
  ):
    | AsyncGenerator<
        {
          chunk: string;
          serverSentEvents: boolean;
        },
        void,
        void
      >
    | undefined {
    if (!this.#token) {
      throw new Error('Token needs to be provided to stream code suggestions');
    }

    const headers = {
      ...this.#getDefaultHeaders(this.#token),
      'Content-Type': 'application/json',
      'X-Supports-Sse-Streaming': 'true',
    };

    const response = await this.#lsFetch.fetch(
      `${this.#baseURL}/api/v4/code_suggestions/completions`,
      {
        method: 'POST',
        headers,
        body: JSON.stringify(request),
      },
    );
    await handleFetchError(response, 'Code Suggestions - Generation');

    const isSse = response.headers.get('X-Streaming-Format') === 'sse';
    for await (const chunk of this.#lsFetch.streamResponse(response, cancellationToken)) {
      yield { chunk, serverSentEvents: isSse };
    }
  }

  async fetchFromApi<TReturnType>(request: ApiRequest<TReturnType>): Promise<TReturnType> {
    if (!this.#token) {
      return Promise.reject(new Error('Token needs to be provided to authorise API request.'));
    }

    if (
      request.supportedSinceInstanceVersion &&
      !(await this.#instanceVersionHigherOrEqualThen(
        request.supportedSinceInstanceVersion.version,
        this.#baseURL,
        this.#token,
      ))
    ) {
      return Promise.reject(
        new InvalidInstanceVersionError(
          `Can't ${request.supportedSinceInstanceVersion.resourceName} until your instance is upgraded to ${request.supportedSinceInstanceVersion.version} or higher.`,
        ),
      );
    }

    return this.getSimpleClient(this.#baseURL, this.#token).fetchFromApi(request);
  }

  async connectToCable(): Promise<ActionCableCable> {
    if (!this.#token) {
      throw new Error('Token is not set up. Cannot connect to cable without a token.');
    }
    const headers = this.#getDefaultHeaders(this.#token);

    const websocketOptions = {
      headers: {
        ...headers,
        Origin: this.#getOrigin(),
      },
    };

    return connectToCable(this.#baseURL, websocketOptions);
  }

  /** @deprecated Use SimpleApiClient.getDefaultHeaders() */
  #getDefaultHeaders(token: string) {
    return this.getSimpleClient(this.#baseURL, token).getDefaultHeaders();
  }

  async #instanceVersionHigherOrEqualThen(
    version: string,
    baseUrl: string,
    token: string,
  ): Promise<boolean> {
    let instanceVersion = this.#instanceInfo?.instanceVersion;

    // FIXME: these lines don't make sense, we already have the instance version if the API reconfiguration was successful, and if it wasn't, this should not be called
    if (!instanceVersion || baseUrl !== this.#baseURL) {
      instanceVersion = await this.#getGitLabInstanceVersion(baseUrl, token);
    }

    if (!instanceVersion) return false;
    return semverCompare(instanceVersion, version) >= 0;
  }

  #getOrigin(): string | undefined {
    try {
      return new URL(this.#baseURL).origin;
    } catch (error) {
      log.error(
        `Failed to extract origin from baseURL: "${this.#baseURL}". Ensure it is a valid URL.`,
        error,
      );
      return undefined;
    }
  }

  async #getGitLabInstanceVersion(baseUrl: string, token?: string): Promise<string> {
    if (!token) {
      return '';
    }

    const { version } = await this.getSimpleClient(baseUrl, token).fetchFromApi(versionRequest);
    return version;
  }
}
