import {
  RpcMessageDefinition,
  RpcMessageDefinitionProvider,
  ServerToClientRpcMessageDefinitionSource,
} from '@gitlab-org/rpc';

import { Container } from '@gitlab/needle';
import { Logger, withPrefix } from '@gitlab-org/logging';

export class DefaultServerToClientRpcMessageDefinitionProvider
  implements RpcMessageDefinitionProvider
{
  readonly #container: Container;

  readonly #logger: Logger;

  constructor(container: Container, logger: Logger) {
    this.#container = container;
    this.#logger = withPrefix(logger, '[DefaultServerToClientMessageDefinitionProvider]: ');
  }

  getMessageDefinitions(): RpcMessageDefinition[] {
    const definitions = this.#container
      .getAll(ServerToClientRpcMessageDefinitionSource)
      .flatMap((source) => source.getMessageDefinitions());

    this.#logger.debug(
      `Found ${definitions.length} server-to-client message definitions from sources`,
    );

    return definitions;
  }
}
