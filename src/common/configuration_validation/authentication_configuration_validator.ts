import { createInterfaceId, Injectable } from '@gitlab/needle';
import { ClientConfig } from '../config_service';
import {
  AUTHENTICATION,
  AUTHENTICATION_REQUIRED,
  FeatureState,
  FeatureStateCheck,
  INVALID_TOKEN,
  StateCheckId,
} from '../feature_state';
import { GitLabApiClient } from '../api';
import { ConfigurationValidator } from './configuration_validator';

export type AuthenticationConfigurationValidator = ConfigurationValidator;

export const AuthenticationConfigurationValidator =
  createInterfaceId<AuthenticationConfigurationValidator>('AuthenticationConfigurationValidator');

@Injectable(AuthenticationConfigurationValidator, [GitLabApiClient])
export class DefaultAuthenticationConfigurationValidator
  implements AuthenticationConfigurationValidator
{
  #api: GitLabApiClient;

  constructor(api: GitLabApiClient) {
    this.#api = api;
  }

  feature = AUTHENTICATION;

  async validate(config: ClientConfig): Promise<FeatureState> {
    const engagedCheck = await this.#getEngagedCheck(config);

    if (engagedCheck) {
      return {
        featureId: AUTHENTICATION,
        engagedChecks: [engagedCheck],
      };
    }

    return { featureId: AUTHENTICATION, engagedChecks: [] };
  }

  async #getEngagedCheck(
    config: ClientConfig,
  ): Promise<FeatureStateCheck<StateCheckId> | undefined> {
    const { baseUrl, token } = config;

    if (!token || !baseUrl) {
      return {
        checkId: AUTHENTICATION_REQUIRED,
        details: 'You need to authenticate to use GitLab Duo.',
        engaged: true,
      };
    }

    const result = await this.#api.checkToken(baseUrl, token);

    if (!result.valid) {
      return {
        checkId: INVALID_TOKEN,
        details: result.message,
        engaged: true,
      };
    }

    return undefined;
  }
}
