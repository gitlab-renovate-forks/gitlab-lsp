import { ByteSizeLimitPreProcessor } from './byte_size_limit';
import { DisabledItemPreProcessor } from './disabled_item';
import { EmptyContentPreProcessor } from './empty_content';
import { SupportedLanguagePreProcessor } from './supported_language';

export const preProcessorContributions = [
  DisabledItemPreProcessor,
  EmptyContentPreProcessor,
  ByteSizeLimitPreProcessor,
  SupportedLanguagePreProcessor,
] as const;
