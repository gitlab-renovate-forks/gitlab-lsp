import { ApiReconfiguredData, ApiRequest, GitLabApiService } from '@gitlab-org/core';
import { Cable } from '@anycable/core';
import { Injectable } from '@gitlab/needle';
import { Disposable } from '@gitlab-org/disposable';
import { GitLabApiClient } from './api';

@Injectable(GitLabApiService, [GitLabApiClient])
export class ProxyGitLabApiService implements GitLabApiService {
  #client: GitLabApiClient;

  constructor(client: GitLabApiClient) {
    this.#client = client;
  }

  fetchFromApi<TReturnType>(request: ApiRequest<TReturnType>): Promise<TReturnType> {
    return this.#client.fetchFromApi(request);
  }

  connectToCable(): Promise<Cable> {
    return this.#client.connectToCable();
  }

  onApiReconfigured(listener: (data: ApiReconfiguredData) => void): Disposable {
    return this.#client.onApiReconfigured(listener);
  }

  get instanceInfo() {
    return this.#client.instanceInfo;
  }

  get tokenInfo() {
    return this.#client.tokenInfo;
  }
}
