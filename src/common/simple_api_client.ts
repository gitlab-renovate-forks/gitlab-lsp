import { ApiRequest } from '@gitlab-org/core';
import { GraphQLClient } from 'graphql-request';
import type { Variables, RequestDocument } from 'graphql-request';
import { createQueryString, QueryValue } from './utils/create_query_string';
import { LsFetch } from './fetch';
import { ClientInfo } from './tracking/code_suggestions/code_suggestions_tracking_types';
import { getLanguageServerVersion } from './utils/get_language_server_version';
import { getUserAgent } from './utils/get_user_agent';
import { handleFetchError } from './handle_fetch_error';

const getResourceName = (path: string) => path.split('/').pop() || 'unknown resource';

export interface SimpleApiClient {
  getDefaultHeaders(): Record<string, string>;
  fetchFromApi<TReturnType>(request: ApiRequest<TReturnType>): Promise<TReturnType>;
}

export class DefaultSimpleApiClient implements SimpleApiClient {
  #lsFetch: LsFetch;

  #baseUrl: string;

  #token: string;

  #clientInfo?: ClientInfo;

  constructor(
    lsFetch: LsFetch,
    clientInfo: ClientInfo | undefined,
    baseUrl: string,
    token: string,
  ) {
    this.#lsFetch = lsFetch;
    this.#baseUrl = baseUrl;
    this.#token = token;
    this.#clientInfo = clientInfo;
  }

  getDefaultHeaders() {
    return {
      Authorization: `Bearer ${this.#token}`,
      'User-Agent': getUserAgent(this.#clientInfo),
      'X-Gitlab-Language-Server-Version': getLanguageServerVersion(),
    };
  }

  async fetchFromApi<TReturnType>(request: ApiRequest<TReturnType>): Promise<TReturnType> {
    if (request.type === 'graphql') {
      return this.#graphqlRequest(request.query, request.variables, request.signal);
    }
    switch (request.method) {
      case 'GET':
        return this.#fetch(request.path, request.searchParams, request.headers, request.signal);
      case 'POST':
        return this.#postFetch(request.path, request.body, request.headers, request.signal);
      case 'PATCH':
        return this.#patchFetch(request.path, request.body, request.headers, request.signal);
      default:
        // the type assertion is necessary because TS doesn't expect any other types
        throw new Error(`Unknown request type ${(request as ApiRequest<unknown>).type}`);
    }
  }

  async #graphqlRequest<T = unknown, V extends Variables = Variables>(
    document: RequestDocument,
    variables?: V,
    signal?: AbortSignal,
  ): Promise<T> {
    const ensureEndsWithSlash = (url: string) => url.replace(/\/?$/, '/');
    const endpoint = new URL('./api/graphql', ensureEndsWithSlash(this.#baseUrl)).href; // supports GitLab instances that are on a custom path, e.g. "https://example.com/gitlab"
    const graphqlFetch = async (
      input: RequestInfo | URL,
      init?: RequestInit,
    ): Promise<Response> => {
      const url = input instanceof URL ? input.toString() : input;
      return this.#lsFetch.post(url, {
        ...init,
        headers: { ...init?.headers },
        signal,
      });
    };

    const client = new GraphQLClient(endpoint, {
      fetch: graphqlFetch,
      headers: { ...this.getDefaultHeaders() },
    });
    return client.request(document, variables);
  }

  async #fetch<T>(
    apiResourcePath: string,
    query: Record<string, QueryValue> = {},
    headers?: Record<string, string>,
    signal?: AbortSignal,
  ): Promise<T> {
    const url = `${this.#baseUrl}${apiResourcePath}${createQueryString(query)}`;

    const result = await this.#lsFetch.get(url, {
      headers: { ...this.getDefaultHeaders(), ...headers },
      signal,
    });

    await handleFetchError(result, getResourceName(apiResourcePath));

    return result.json() as Promise<T>;
  }

  async #postFetch<T>(
    apiResourcePath: string,
    body?: unknown,
    headers?: Record<string, string>,
    signal?: AbortSignal,
  ): Promise<T> {
    const url = `${this.#baseUrl}${apiResourcePath}`;

    const response = await this.#lsFetch.post(url, {
      headers: {
        'Content-Type': 'application/json',
        ...this.getDefaultHeaders(),
        ...headers,
      },
      body: JSON.stringify(body),
      signal,
    });

    await handleFetchError(response, getResourceName(apiResourcePath));

    return response.json() as Promise<T>;
  }

  async #patchFetch<T>(
    apiResourcePath: string,
    body?: unknown,
    headers?: Record<string, string>,
    signal?: AbortSignal,
  ): Promise<T> {
    const url = `${this.#baseUrl}${apiResourcePath}`;

    const response = await this.#lsFetch.patch(url, {
      headers: {
        'Content-Type': 'application/json',
        ...this.getDefaultHeaders(),
        ...headers,
      },
      body: JSON.stringify(body),
      signal,
    });

    await handleFetchError(response, getResourceName(apiResourcePath));

    return response.json() as Promise<T>;
  }
}
