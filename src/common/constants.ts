export const REQUEST_TIMEOUT_MILLISECONDS = 15000;
export const GITLAB_API_BASE_URL = 'https://gitlab.com';
export const SUGGESTION_ACCEPTED_COMMAND = 'gitlab.ls.codeSuggestionAccepted';
export const START_STREAMING_COMMAND = 'gitlab.ls.startStreaming';
export const SUGGESTIONS_DEBOUNCE_INTERVAL_MS = 250;
