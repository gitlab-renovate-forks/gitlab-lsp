import { GetRequest } from '@gitlab-org/core';

export type ResolutionStrategy = 'open_tabs' | 'imports';

export type AdditionalContext = {
  /**
   * The type of the context element. Options: `file` or `snippet`.
   */
  type: 'file' | 'snippet';
  /**
   * The name of the context element. A name of the file or a code snippet.
   */
  name: string;
  /**
   * The content of the context element. The body of the file or a function.
   */
  content: string;
  /**
   * The list of all sources where the context was derived from
   */
  resolution_strategies: ResolutionStrategy[];
};

interface ISuggestionOptionModel {
  lang?: string;
  engine?: string;
  name?: string;
}

// FIXME: Rename to SuggestionOptionText and then rename SuggestionOptionOrStream to SuggestionOption
// this refactor can't be done now (2024-05-28) because all the conflicts it would cause with WIP
export interface SuggestionOption {
  index?: number;
  text: string;
  uniqueTrackingId: string;
  model?: ISuggestionOptionModel;
}

export interface TelemetryRequest {
  event: string;
  additional_properties: {
    unique_tracking_id: string;
    language?: string;
    suggestion_size?: number;
    timestamp: string;
  };
}

export type GitLabProjectId = number;

export type GitLabVersionResponse = {
  version: string;
  enterprise?: boolean;
};

export const versionRequest: GetRequest<GitLabVersionResponse> = {
  type: 'rest',
  method: 'GET',
  path: '/api/v4/version',
};
