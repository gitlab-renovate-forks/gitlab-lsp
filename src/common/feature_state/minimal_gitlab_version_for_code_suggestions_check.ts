import { EventEmitter } from 'events';
import { Disposable } from '@gitlab-org/disposable';
import { createInterfaceId, Injectable } from '@gitlab/needle';
import { ConfigService, ClientConfig } from '../config_service';
import { versionRequest } from '../api_types';
import { GitLabApiClient } from '../api';
import { ifVersionGte } from '../utils/if_version_gte';
import {
  FeatureStateCheck,
  StateCheckId,
  UNSUPPORTED_GITLAB_VERSION,
  UnsupportedGitLabVersionCheckContext,
} from './feature_state_management_types';
import { StateCheck, StateCheckChangedEventData, StateConfigCheck } from './state_check';

export const MINIMUM_CODE_SUGGESTIONS_VERSION = '16.8.0';

export type CodeSuggestionsInstanceVersionCheck = StateCheck<typeof UNSUPPORTED_GITLAB_VERSION> &
  StateConfigCheck;

export const CodeSuggestionsInstanceVersionCheck =
  createInterfaceId<CodeSuggestionsInstanceVersionCheck>('CodeSuggestionsInstanceVersionCheck');

@Injectable(CodeSuggestionsInstanceVersionCheck, [GitLabApiClient, ConfigService])
export class DefaultCodeSuggestionsInstanceVersionCheck
  implements CodeSuggestionsInstanceVersionCheck
{
  #subscriptions: Disposable[] = [];

  #stateEmitter = new EventEmitter();

  #api: GitLabApiClient;

  #isVersionDeprecated = false;

  #token?: string;

  #baseUrl?: string;

  #instanceVersion?: string;

  details?: string;

  context?: UnsupportedGitLabVersionCheckContext;

  constructor(api: GitLabApiClient, configService: ConfigService) {
    this.#api = api;

    this.#subscriptions.push(
      configService.onConfigChange(async (config) => {
        const { token, baseUrl } = config.client;
        // FIXME: When configService has `affectsConfiguration` implemented - use that instead
        // https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp/-/issues/405
        // to avoid multiple API calls when neither token or baseUrl has changed
        if (baseUrl && token && (this.#baseUrl !== baseUrl || this.#token !== token)) {
          this.#baseUrl = baseUrl;
          this.#token = token;
          await this.#checkInstanceVersion();
        }
      }),
    );
  }

  async #checkInstanceVersion(): Promise<void> {
    const response = await this.#api.fetchFromApi(versionRequest);

    const { version } = response;

    if (!version) {
      return;
    }

    this.#instanceVersion = version;

    ifVersionGte(
      version,
      MINIMUM_CODE_SUGGESTIONS_VERSION,
      () => {
        this.#isVersionDeprecated = false;
        this.#stateEmitter.emit('change', this);
      },
      () => {
        this.#isVersionDeprecated = true;
        this.details = `GitLab Duo Code Suggestions requires GitLab version 16.8 or later. GitLab instance located at: ${this.#baseUrl} is currently using ${this.#instanceVersion}`;
        this.context = {
          // we check that baseUrl is available before calling current method
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          baseUrl: this.#baseUrl!,
          version,
        };
        this.#stateEmitter.emit('change', this);
      },
    );
  }

  onChanged(listener: (data: StateCheckChangedEventData) => void): Disposable {
    this.#stateEmitter.on('change', listener);
    return {
      dispose: () => this.#stateEmitter.removeListener('change', listener),
    };
  }

  get engaged() {
    return this.#isVersionDeprecated;
  }

  id = UNSUPPORTED_GITLAB_VERSION;

  dispose() {
    this.#subscriptions.forEach((s) => s.dispose());
  }

  async validate(config: ClientConfig): Promise<FeatureStateCheck<StateCheckId> | undefined> {
    if (!config.baseUrl) {
      return {
        checkId: this.id,
        details: 'Config is missing GitLab API URL (`baseUrl` parameter)',
        engaged: true,
      };
    }
    if (!config.token) {
      return {
        checkId: this.id,
        details: 'Config is missing GitLab API token (`token` parameter)',
        engaged: true,
      };
    }

    const { version } = await this.#api
      .getSimpleClient(config.baseUrl, config.token)
      .fetchFromApi(versionRequest);

    return ifVersionGte<FeatureStateCheck<StateCheckId> | undefined>(
      version,
      MINIMUM_CODE_SUGGESTIONS_VERSION,
      () => undefined,
      () => ({
        checkId: this.id,
        details: `GitLab Duo Code Suggestions requires GitLab version 16.8 or later. Current version is ${version}.`,
        engaged: true,
      }),
    );
  }
}
